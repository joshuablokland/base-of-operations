<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Base of Operations</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/dist.main.css">
        <script src="assets/js/vendor/modernizr.js"></script>
    </head>
    <body>

        <p>Base of Operations</p>

        <script src="assets/js/dist.main.js"></script>

    </body>
</html>
