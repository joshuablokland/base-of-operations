"use strict";

// ------------------------------------------------------------------------------------------
//  1. Gulp tasks for development
//  2. Tasks to distribute
//  3. Gulp default ( run: $ gulp )
//     Maks use of "1. Gulp tasks for development"
// ------------------------------------------------------------------------------------------

var gulp            = require('gulp'),
    sass            = require('gulp-sass'),
    concat          = require('gulp-concat'),
    concatCSS       = require('gulp-concat-css'),
    uglify          = require('gulp-uglify'),
    jshint          = require('gulp-jshint'),
    stylish         = require('jshint-stylish'),
    notify          = require('gulp-notify'),
    modernizr       = require('gulp-modernizr'),
    cleanCSS        = require('gulp-clean-css'),
    babel           = require("gulp-babel"),
    livereload      = require("gulp-livereload"),
    bower_scripts   = require('./bower_scripts.json');

var paths = {
    sass: '../sass/**/*.scss',
    scripts: ['../js/**/*.js', '!../js/*.min.js', '!../js/{dist,dist/**}', '!../js/{vendor,vendor/**}'],
    dev_scripts: '../js',
    vendor_scripts: '../js/vendor',
    dist_css: '../css'
};



// ------------------------------------------------------------------------------------------
//  1. Gulp tasks
//      | terminal codes
//      | -------------------
//      | gulp build-js-modernizr
//      | gulp build-js-plugins
//      | gulp lint
//      | gulp js
//      | gulp sass
//      | gulp watch
// ------------------------------------------------------------------------------------------

gulp.task('build-js-modernizr', function() {
    gulp.src('../js/*.js')
        .pipe(modernizr())
        .pipe(gulp.dest(paths.vendor_scripts));
});

gulp.task('build-js-plugins', function () {
    return gulp.src(bower_scripts)
        .pipe(concat('plugins.min.js'))
        .pipe(gulp.dest(paths.dev_scripts));
});

gulp.task('lint', function() {
    return gulp.src(paths.scripts)
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
});

gulp.task('js', function () {
    return gulp.src(paths.scripts)
        .pipe(babel())
        .pipe(concat('main.min.js'))
        .pipe(gulp.dest(paths.dev_scripts));
});

gulp.task('sass', function () {
    return gulp.src(paths.sass)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(paths.dist_css))
        .pipe(notify('SASS compiled to CSS'))
        .pipe(livereload());
});

gulp.task('watch', function () {
    livereload.listen();
    gulp.watch(paths.sass, ['sass']);
    gulp.watch(paths.scripts, ['lint', 'js']);
});



// ------------------------------------------------------------------------------------------
//  2. Tasks to build / dist
//      | terminal codes
//      | -------------------
//      | gulp dist-js
//      | gulp dist-css
// ------------------------------------------------------------------------------------------

gulp.task('dist-js', function () {
    return gulp.src( ['../js/plugins.min.js', '../js/main.min.js'] )
        .pipe(concat('dist.main.js'))
        .pipe(uglify())
        .pipe(gulp.dest('../js'));
});

gulp.task('dist-css', function() {
    return gulp.src('../css/*.css')
        .pipe(concatCSS('dist.style.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest(paths.dist_css));
});

gulp.task('dist', ['dist-js', 'dist-css']);



// ------------------------------------------------------------------------------------------
//  3. Gulp default
//      | terminal codes
//      | -------------------
//      | gulp
// ------------------------------------------------------------------------------------------

gulp.task('default', ['watch']);
