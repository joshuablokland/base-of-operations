# Base of Operations #

Base of Operations (BOO) is way to keep all your plugins, compilers and development operations separated. 
This is a basic setup I use for front-end purposes. 

## Includes support for ##
SASS, ES6

## How do I get set up? ##

#### Step 1: Node JS ####

[Download & install Node JS](https://nodejs.org/en/)

#### Step 2: Install npm packages ####

` $ npm install `

#### Step 3: Install bower packages ####

` $ bower install `

#### Step 4: Install bower packages ####

Download the BOO boilerplate

## Gulp commands ##

#### Build a Modernizr JS file ####

` $ gulp build-js-modernizr `

#### Build a Plugins file with Bower files as a base ####

` $ gulp build-js-plugins `

#### Hardcore development mode ####

` $ gulp `

#### Distribute files, this will minify CSS and JS files ####

` $ gulp dist `